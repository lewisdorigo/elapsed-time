<?php namespace Dorigo\Time\Test;

use PHPUnit\Framework\TestCase;
use Dorigo\Time;

class TimeTest extends TestCase {

    public function testReturnsCorrectSeconds() {
        $this->assertSame(Time::timeFromSeconds(1), "1 second");
        $this->assertSame(Time::timeFromSeconds(10), "10 seconds");
    }

    public function testReturnsCorrectMinutes() {
        $this->assertSame(Time::timeFromSeconds(60), "1 minute");
        $this->assertSame(Time::timeFromSeconds(600), "10 minutes");
    }

    public function testReturnsCorrectDays() {
        $this->assertSame(Time::timeFromSeconds(86400), "1 day");
        $this->assertSame(Time::timeFromSeconds(864000), "10 days");
    }

    public function testWordPressReadTime() {
        $this->assertSame(Time::wpPostReadTime(), "0 seconds");
    }
}