<?php namespace Dorigo;

class Time {
    private static $times = [
        365 * 24 * 60 * 60  =>  'year',
         30 * 24 * 60 * 60  =>  'month',
              24 * 60 * 60  =>  'day',
                   60 * 60  =>  'hour',
                        60  =>  'minute',
                         1  =>  'second'
    ];

    private static $shortLabels = [
        'year'   => 'yr',
        'month'  => 'mon',
        'day'    => 'd',
        'hour'   => 'hr',
        'minute' => 'min',
        'second' => 'sec'
    ];

    public static function elapsed(int $timestamp) {
        $elapsed = time() - $timestamp;

        if($elapsed < 1) { return "0 seconds ago"; }

        return self::timeFromSeconds($elapsed);
    }

    public static function timeFromSeconds(int $seconds, bool $useLargest = true, bool $singularLabel = false, bool $shortLabels = false) {
        if($seconds < 1) {
            return "0 ".($shortLabels?'s':'seconds');
        }

        $return = [];

        foreach(self::$times as $numSeconds => $label) {
            $currentTime = $seconds / $numSeconds;

            if($currentTime >= 1) {
                $round = round($currentTime);
                $seconds = $seconds - ($numSeconds * $round);

                $return[] = $round.' '.($shortLabels?self::$shortLabels[$label]:$label).($round>1&&!$singularLabel?'s':'');

                if($useLargest) { break; }
            }
        }

        return $useLargest ? $return[0] : implode(', ', $return);
    }

    public static function wpPostReadTime($post_id = null, $wpm = 240, $singular = true) {
        if(!function_exists("get_the_content")) {
            return self::timeFromSeconds(0, true, $singular);
        }

        $post = get_post($post_id);

        $content = $post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = strip_tags($content);

        $word_count = str_word_count($content);

        return self::timeFromSeconds($word_count / ($wpm / 60), true, $singular);
    }
}